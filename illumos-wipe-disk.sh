#!/usr/bin/ksh93

# Wipe disks in order to re-use them from previous zpool
# This should work on any illumos/solaris host.
# Tested on OmnisOS r151026 

# Version 0.1 - 20180907

# This script only wipe same-sized disks for now
SIZE="10000831348736"
LAST_PART=`echo "($SIZE / 1024 / 1024) - 10" | /usr/bin/bc`

# List of disks to wipe
DRIVES="c0t5000CCA26BD0CAFAd0p0 c0t5000CCA26BD116ACd0p0 c0t5000CCA26BD59F6Dd0p0 c0t5000CCA26BD5AAC5d0p0
c0t5000CCA26BD6960Ed0p0 c0t5000CCA26BD6B9CCd0p0 c0t5000CCA26BD6C6D4d0p0 c0t5000CCA26BD6E59Cd0p0"


##############
# verifydrives
##############

verifydrives()
  {
    for DRIVE in $DRIVES
      do
        if [ `/bin/ls -1 /dev/rdsk/$DRIVE` ]
          then
            echo "Drive $DRIVE verified."
          else
            echo "Drive /dev/rdsk/$DRIVE does not exists. Aborting"
            exit
        fi
      done
  }

verifydrives


#############
# cleandrives
#############

cleandrives()
  {
        for DRIVE in $DRIVES
    do
        echo "Wiping disk /dev/rdsk/$DRIVE"
        echo ""
        echo "Wipe beginning of disk"
        dd if=/dev/zero of=/dev/rdsk/${DRIVE} bs=1M count=10 #>/dev/null 2>&1
        echo ""
        echo "Wipe last part of disk"
        dd if=/dev/zero of=/dev/rdsk/${DRIVE} bs=1M count=10 seek=$LAST_PART
        echo "$DRIVE done"
        echo ""
    done
  }


echo ""
echo "This will irreversibly destroy partition- and filesystem data on drive(s):"
echo "$DRIVES"
echo ""
echo "USE WITH EXTREME CAUTION!"
echo Do you confirm "yes/no":
read choice
  case "$choice" in
    yes|Yes|y) cleandrives $DRIVES
         echo ""
         echo "Drive(s) cleaned."  ;;
     no) echo ""
         echo "Cleaning cancelled."; break ;;
      *) echo ""
         echo "Cleaning cancelled."; break ;;
  esac
